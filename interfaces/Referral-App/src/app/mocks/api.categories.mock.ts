export default [
  {
    categoryID: 1,
    categoryName: { en: 'Shelter' },
    categoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    categoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
  },
  {
    categoryID: 2,
    categoryName: { en: 'Lawyer' },
    categoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    categoryDescription: { en: ' ' },
  },
  {
    categoryID: 3,
    categoryName: { en: 'Doctor' },
    categoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    categoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
  },
];
