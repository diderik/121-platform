export default [
  {
    offerID: 1,
    offerName: { en: 'Wereld Huis' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.wereldhuis.org',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 1,
    categoryID: 1,
  },
  {
    offerID: 2,
    offerName: { en: 'Boost' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.boostamsterdam.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 1,
    categoryID: 1,
  },
  {
    offerID: 3,
    offerName: { en: 'Hvoquerido' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.hvoquerido.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 2,
    categoryID: 1,
  },
  {
    offerID: 4,
    offerName: { en: 'Leger des Heils' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.legerdesheils.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 2,
    categoryID: 1,
  },
  {
    offerID: 5,
    offerName: { en: 'Gemeente Amsterdam' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.amsterdam.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 2,
    categoryID: 1,
  },
  {
    offerID: 6,
    offerName: { en: 'Ask V' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.amsterdam.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 2,
    categoryID: 1,
  },
  {
    offerID: 7,
    offerName: { en: 'Harriet Tubman Huis' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.harriettubmanhuis.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 3,
    categoryID: 1,
  },
  {
    offerID: 8,
    offerName: { en: 'Janette Noelhuis' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.noelhuis.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 3,
    categoryID: 1,
  },
  {
    offerID: 9,
    offerName: { en: 'Vluchtlingen Werk' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.vluchtlingenwerk.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 4,
    categoryID: 2,
  },
  {
    offerID: 10,
    offerName: { en: 'Juridischolket' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.juridischolket.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 4,
    categoryID: 2,
  },
  {
    offerID: 11,
    offerName: { en: 'Ask V' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.askv.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 4,
    categoryID: 2,
  },
  {
    offerID: 12,
    offerName: { en: 'Gemeente Amsterdam' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.amsterdam.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 4,
    categoryID: 2,
  },
  {
    offerID: 13,
    offerName: { en: 'Advocaten Spreekuur' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.advocatenspreekuur.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 4,
    categoryID: 2,
  },
  {
    offerID: 14,
    offerName: { en: 'Dokters van de Wereld' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.doktersvandewereld.org',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 5,
    categoryID: 3,
  },
  {
    offerID: 15,
    offerName: { en: 'Oude Zijds 100' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.oudezijds100.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 5,
    categoryID: 3,
  },
  {
    offerID: 16,
    offerName: { en: 'Spoedeisende Psychiatrie' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.spoedeisendepsychiatrieamsterdam.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 6,
    categoryID: 3,
  },
  {
    offerID: 17,
    offerName: { en: 'Oude Zijds 100' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.oudezijds100.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 6,
    categoryID: 3,
  },
  {
    offerID: 18,
    offerName: { en: 'i-psy' },
    offerIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    offerDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    offerLink: 'www.i-psy.nl',
    offerImage:
      'https://www.510.global/wp-content/uploads/2019/04/510-ALL-DISASTER-PHASES-DIRECT-DIGITAL-AID-2-1200x600.png',
    subCategoryID: 6,
    categoryID: 3,
  },
];
