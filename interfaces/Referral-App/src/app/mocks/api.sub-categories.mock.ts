export default [
  {
    subCategoryID: 1,
    subCategoryName: { en: 'Day Shelter' },
    subCategoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    subCategoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    categoryID: 1,
  },
  {
    subCategoryID: 2,
    subCategoryName: { en: 'Night Shelter' },
    subCategoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    subCategoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    categoryID: 1,
  },
  {
    subCategoryID: 3,
    subCategoryName: { en: 'Women and Child Night Shelter' },
    subCategoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    subCategoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    categoryID: 1,
  },
  {
    subCategoryID: 4,
    subCategoryName: { en: 'Advocaten' },
    subCategoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    subCategoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    categoryID: 2,
  },
  {
    subCategoryID: 5,
    subCategoryName: { en: 'Medical Doctor' },
    subCategoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    subCategoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    categoryID: 3,
  },
  {
    subCategoryID: 6,
    subCategoryName: { en: 'Psychological Support' },
    subCategoryIcon:
      'https://www.510.global/wp-content/uploads/2017/07/510-LOGO-WEBSITE-01.png',
    subCategoryDescription: {
      en:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    },
    categoryID: 3,
  },
];
