export default {
  helpIcon: 'assets/icons/help.png',
  helpText: {
    en: 'Mock Help Text',
  },
  helpPhoneLabel: {
    en: 'Mock Phone Label',
  },
  helpPhone: 'Mock Phone Number',
  helpWhatsApp: 'Mock WhatsApp Link',
  helpTwitter: 'Mock Twitter Link',
  helpFacebook: 'Mock Facebook Link',
};
