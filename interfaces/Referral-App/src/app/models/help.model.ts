import { TranslatableString } from './translatable-string.model';

export class Help {
  helpIcon: string;
  helpText: TranslatableString | string;
  helpPhoneLabel: TranslatableString | string;
  helpPhone: string;
  helpWhatsApp: string;
  helpTwitter: string;
  helpFacebook: string;
}
